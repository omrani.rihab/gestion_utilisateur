<?php
    require_once('role.php');
    require_once("connexiondb.php");
    $login=isset($_GET['login'])?$_GET['login']:"";
    
    $size=isset($_GET['size'])?$_GET['size']:3;
    $page=isset($_GET['page'])?$_GET['page']:1;
    $offset=($page-1)*$size;
   
    $requeteUser="select * from utilisateur where login like '%$login%'";
    $requeteCount="select count(*) countUser from utilisateur";
   
    $resultatUser=$pdo->query($requeteUser);
    $resultatCount=$pdo->query($requeteCount);

    $tabCount=$resultatCount->fetch();
    $nbrUser=$tabCount['countUser'];
    $reste=$nbrUser % $size;   
    if($reste===0) 
        $nbrPage=$nbrUser/$size;   
    else
        $nbrPage=floor($nbrUser/$size)+1;  
?>
<! DOCTYPE HTML>
<HTML>
    <head>
        <meta charset="utf-8">
        <title>Gestion des utilisateurs</title>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php include("menu.php"); ?>
        
        <div class="container">
            <div class="panel panel-success margetop60">
				<div class="panel-heading">Rechercher des utilisateurs</div>
				<div class="panel-body">
					<form method="get" action="utilisateurs.php" class="form-inline">
						<div class="form-group">
                            <input type="text" name="login" 
                                   placeholder="Login"
                                   class="form-control"
                                   value="<?php echo $login ?>"/>
                        </div>
				        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-search"></span>
                            Chercher...
                        </button> 
					</form>
				</div>
			</div>
            
            <div class="panel panel-primary">
                <div class="panel-heading">Liste des utilisateurs (<?php echo $nbrUser ?> utilisateurs)</div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered" id="tb1">
                        <thead>
                            <tr>
                                <th>login</th> <th>Email</th> <th>Role</th> <th>Actions</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php while($user=$resultatUser->fetch()){ ?>
                                <tr id="<?php echo $user['idUser'] ?>">
                                    <td data-target="login"><?php echo $user['login'] ?> </td>
                                    <td data-target="email"><?php echo $user['email'] ?> </td>
                                    <td data-target="role"><?php echo $user['role'] ?> </td>  
                                   <td>
                                        <a href="#" data-role="update" data-id="<?php echo $user['idUser'] ?>">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        &nbsp;&nbsp;
                                        <a href="#" data-role="delete" data-id="<?php echo $user['idUser'] ?>">
                                                <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                        &nbsp;&nbsp;
                                        </td>       
                                </tr>
                             <?php } ?>
                        </tbody>
                    </table>
                <div>
                    <ul class="pagination">
                        <?php for($i=1;$i<=$nbrPage;$i++){ ?>
                            <li class="<?php if($i==$page) echo 'active' ?>"> 
            <a href="utilisateurs.php?page=<?php echo $i;?>&login=<?php echo $login ?>">
                                    <?php echo $i; ?>
                                </a> 
                             </li>
                        <?php } ?>
                    </ul>
                </div>

                
                </div>
            </div>
            <div id="inner"></div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edition de l'utilisateur :</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                     <label for="login">Login :</label>
                     <input id ="login" type="text" name="login" placeholder="Login" class="form-control" value="<?php echo $login ?>"/>
                </div>
                <div class="form-group">
                    <label for="email">Email :</label>
                    <input id="email" type="email" name="email" placeholder="email" class="form-control" value="<?php echo $email ?>"/>

                </div>
                <input type="hidden" id="userId"  class="form-control">
            </div>
            <div class="modal-footer">
                <a href="#" id="save"  class="btn btn-primary pull-right">Enregistrer</a>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>

        <div class="modal fade" id="myModalDelete" role="dialog">
        <div class="modal-dialog">
        
            <!-- Modal delete content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Supprimer de l'utilisateur :</h4>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet utilisateur!</p>
                <input type="hidden" id="userIdDelete"  class="form-control">
            </div>
            <div class="modal-footer">
                <a href="#" id="delete"  class="btn btn-danger pull-right">Supprimer</a>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>


        <div class="modal fade" id="myModalUpdatePassword" role="dialog">
        <div class="modal-dialog">

        <!-- Modal change password-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Changet votre Mot de passe :</h4>
            </div>
            <div class="modal-body">
                <div class="input-container">
                    <input  id ="oldpwd" class="form-control oldpwd"
                        type="password"
                        name="oldpwd"
                        autocomplete="new-password"
                        placeholder="Taper votre Ancien Mot de passe"
                        required>
                    <i class="fa fa-eye fa-2x show-old-pwd clickable"></i>
                </div>

                <div class="input-container">
                    <input id ="newpwd" minlength=4
                            class="form-control newpwd"
                            type="password"
                            name="newpwd"
                            autocomplete="new-password"
                            placeholder="Taper votre Nouveau Mot de passe"
                            required>
                    <i class="fa fa-eye fa-2x show-new-pwd clickable"></i>
              </div>
                <input type="hidden" id="userIdDelete"  class="form-control">
            </div>
            <div class="modal-footer">
                <a href="#" id="changerPwd"  class="btn btn-danger pull-right">Modifier</a>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>
    </body>

    <script>
    $( document ).ready(function() {
        
        //Update user
        $(document).on('click','a[data-role=update]',function(){

            var id = $(this).data('id');
            var login = $('#'+id).children('td[data-target=login]').text();
            var email = $('#'+id).children('td[data-target=email]').text();

            $('#login').val(login);
            $('#email').val(email);
            $('#userId').val(id);
            $('#myModal').modal('toggle');

            $('#save').click(function(){

                var id = $('#userId').val();
                var login = $('#login').val();
                var email = $('#email').val();
               
                $.ajax({
                    url      : "updateUtilisateur.php", 
                    method   : "post",
                    data     : {login: login, email: email, iduser: id},
                    success  : function(response){
                        $('#'+id).children('td[data-target=login]').text(login);
                        $('#'+id).children('td[data-target=email]').text(email);
                        $('#myModal').modal('hide');
                }});

            })

        })

        //Delete user

        $(document).on('click','a[data-role=delete]',function(){

        var id = $(this).data('id');

        $('#userId').val(id);
        $('#myModalDelete').modal('toggle');

        $('#delete').click(function(){
        
            $.ajax({
                url      : "supprimerUtilisateur.php", 
                method   : "post",
                data     : {iduser: id},
                success  : function(response){
                    $('table#tb1 tr#'+id).remove();
                    $('#myModalDelete').modal('hide');
            }});

        })

        })

        //Update password
        $(document).on('click','a[data-role=changer]',function(){
          
        var id = $(this).data('id');
        
        $('#userId').val(id);
        $('#myModalUpdatePassword').modal('toggle');

        $('#changerPwd').click(function(){            
            var oldpwd = $('#oldpwd').val();
            var newpwd = $('#newpwd').val();

            $.ajax({
                url      : "updatePwd.php", 
                method   : "post",
                data     : {iduser: id, oldpwd: oldpwd, newpwd: newpwd},
                success  : function(response){
                    $("#myModalUpdatePassword").modal('hide');
                    $( "#inner" ).empty().append(response );
            }});

            
        })

        })

        });

    </script>    
</HTML>